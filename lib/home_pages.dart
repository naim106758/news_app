

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Model/News/all_news.dart';
import 'Model/News/breaking_news.dart';

class HomePages extends StatefulWidget {
  const HomePages({Key? key}) : super(key: key);

  @override
  State<HomePages> createState() => _HomePagesState();
}

class _HomePagesState extends State<HomePages> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text("flutter News App"),
            bottom: TabBar(tabs: [
              Tab(
                text: 'Bracking News',
              ),
              Tab(
                text: 'All News',
              ),
            ]),
          ),
          body: TabBarView(children: [
            BreakingNews(),
            AllNews(),
          ]),
        ));
  }
}
