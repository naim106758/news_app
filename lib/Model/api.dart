import 'dart:convert';

import 'package:http/http.dart';

import 'News/news_model.dart';
class Api {
  final All_api = 'https://newsapi.org/v2/everything?q=bitcoin&apiKey=60f2e558391b408b8385e124990c74b9';
  final Breaking_api = "https://newsapi.org/v2/top-headlines?country=us&apiKey=60f2e558391b408b8385e124990c74b9";

  Future<List<NewsModel>> getAllNews() async {
    try {
      Response response = await get(Uri.parse(All_api));
      if (response.statusCode == 200) {
        Map<String, dynamic>json = jsonDecode(response.body);
        List<dynamic>body = json['articles'];
        List<NewsModel>articlesList = body.map((item) =>
            NewsModel.fromJson(item)).toList();
        return articlesList;
      } else {
        throw('No News Found');
      }
    } catch (e) {
      throw e;
    }
  }

  Future<List<NewsModel>> getBreakingNews() async {
    try {
      Response response = await get(Uri.parse(Breaking_api));
      if (response.statusCode == 200) {
        Map<String, dynamic>json = jsonDecode(response.body);
        List<dynamic>body = json['articles'];
        List<NewsModel>articlesList = body.map((item) =>
            NewsModel.fromJson(item)).toList();
        return articlesList;
      } else {
        throw('No News Found');
      }
    } catch (e) {
      print(e.toString());
      throw e;
    }
  }
}

