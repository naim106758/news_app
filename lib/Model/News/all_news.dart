import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_apps_api/Model/News/news_item_list.dart';
import '../api.dart';
import 'news_model.dart';

class AllNews extends StatefulWidget {
  const AllNews({Key? key}) : super(key: key);

  @override
  State<AllNews> createState() => _AllNewsState();
}

class _AllNewsState extends State<AllNews> {
  Api api = Api();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: api.getAllNews(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<NewsModel>articlesList =snapshot.data??[];
            return ListView.builder(itemBuilder: (context, index) {
              return NewsItemList(newsModel: articlesList[index],);

            }, itemCount: articlesList.length,
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

