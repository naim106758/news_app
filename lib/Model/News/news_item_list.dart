import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'news_details.dart';
import 'news_model.dart';
class NewsItemList extends StatelessWidget {
  final NewsModel newsModel;
  const NewsItemList({Key? key,required this.newsModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
         Navigator.push(context, MaterialPageRoute(builder: (context)=>NewsDetails(newsModel: newsModel)));
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        margin: EdgeInsets.symmetric(horizontal: 12),
        child: Column(
          crossAxisAlignment:CrossAxisAlignment.start,
          children: [
            CachedNetworkImage(
              height: 250,
              width: double.infinity,
              fit:BoxFit.fitWidth ,
              imageUrl:newsModel.urlToImage.toString(),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            Row(
              children: [
                Container(
                  padding: EdgeInsets.all(6),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Text(newsModel.source!.name.toString(),style: TextStyle(color: Colors.green),),
                ),
                SizedBox(width: 5,),
                Text(newsModel.publishedAt.toString(),)
              ],
            ),
            SizedBox(height: 5,),
            Text(newsModel.author==null?"":"Writeen By " +newsModel.author.toString(),),
            SizedBox(height:7,),
            Text(newsModel.title.toString(),)
          ],
        ),
      ),
    );
  }
}
