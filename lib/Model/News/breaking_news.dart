import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_apps_api/Model/News/news_item_list.dart';
import '../api.dart';
import 'news_model.dart';

class BreakingNews extends StatefulWidget {
  const BreakingNews({Key? key}) : super(key: key);

  @override
  State<BreakingNews> createState() => _BreakingNewsState();
}

class _BreakingNewsState extends State<BreakingNews> {
  Api api = Api();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: api.getBreakingNews(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<NewsModel>articlesList =snapshot.data??[];
            return ListView.builder(itemBuilder: (context, index) {
              return NewsItemList(newsModel: articlesList[index],);

            }, itemCount: articlesList.length,
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

