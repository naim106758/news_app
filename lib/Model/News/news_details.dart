import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'news_model.dart';
class NewsDetails extends StatefulWidget {

  final NewsModel newsModel;
  const NewsDetails({Key? key,required this.newsModel}) : super(key: key);

  @override
  State<NewsDetails> createState() => _NewsDetailsState();
}

class _NewsDetailsState extends State<NewsDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.newsModel.title.toString()),
      ),
      body: Column(
        children: [
            CachedNetworkImage(
              height: 250,
              width: double.infinity,
              fit:BoxFit.fitWidth ,
              imageUrl:widget.newsModel.urlToImage.toString(),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            Row(
              children: [
                Container(
                  child: Text(widget.newsModel.source!.name.toString(),style: TextStyle(color: Colors.green),),
                  padding: EdgeInsets.all(6),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
                SizedBox(width: 5,),
                Text(widget.newsModel.publishedAt.toString(),)
              ],
            ),
            SizedBox(height: 5,),
            Text(widget.newsModel.author==null?"":"Writeen By " +widget.newsModel.author.toString(),),
            SizedBox(height:7,),
            Text(widget.newsModel.description.toString(),)

        ],
      ),
    );
  }
}
